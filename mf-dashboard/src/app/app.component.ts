import { Component } from '@angular/core';

@Component({
  selector: 'mf-dashboard',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mf-dashboard';
}
